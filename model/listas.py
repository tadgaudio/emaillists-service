#encoding: utf-8
__author__ = 'tadgaudio@gmail.com (Tadeu luis Pires Gaudio)'
from google.appengine.ext import ndb
from libs.basemodel import BaseModel
from libs import utils
from random import randint
import json


class EmailLista(utils.ModelUtils,BaseModel):
    """
    Aqui permite colocar o e-mail em uma lista específica para seguimentação
    Por exemplo:
       compradores - tadeu@meusalo.com.br - tadeu luis
    """
    #uid = ndb.ComputedProperty(lambda self: self.key.id(), indexed=False)
    lista = ndb.KeyProperty('Lista', required=True)
    email = ndb.StringProperty(required=True)
    nome = ndb.StringProperty()
    sexo = ndb.StringProperty()
    extra = ndb.JsonProperty()

    @classmethod
    def save_email(self, **params):
        """
        Este método salva apenas 1 e-mail em uma lista, retorna a chave se der certo.
        """
        #para fazer multi_put
        # if 'multi' in params:
        #     pass


        _params = self.transform_input_data(params)
        print "Parametros do email {}".format(_params)
        if not 'extra' in params and len(params) > 3:
            _params['extra'] = dict(set(params.items()) - set(_params.items()))
            print "Parametros do email depois do extra params {}".format(_params)
            del _params['extra']['user_id']
            del _params['extra']['popup_id']
            del _params['extra']['lista']
            del _params['extra']['loja_id']
            del _params['extra']['bt-submit']


        email_registrado = self.query(EmailLista.email == _params['email'],
                                          EmailLista.lista == _params['lista']).get()
        novo = False
        if not email_registrado:
            try:
                email_id = "email-%s" % str(randint(0,9999999999999))
                email_registrado = self.get_or_create(email_id, **_params)
                novo = True
            except Exception, e:
                print e
                print "erro ao tentar salvar email"

        return email_registrado, novo

    @classmethod
    def save_emails(self, **params):
        """
        Este método pode receber uma lista de e-mails
        para melhorar os métodos de performance
        https://cloud.google.com/appengine/docs/standard/python/ndb/async
        """
        emails_criados = []
        for email in params['emails']:
            email_registrado = self.query(EmailLista.email == email['email'],
                                          EmailLista.lista == params['lista'].key).fetch()
            if not email_registrado:
                try:
                    email['lista'] = params['lista'].key
                    _params = self.transform_input_data(email)
                    email_id = "email-%s" % str(randint(0,9999999999999))
                    emails_criados.append(self.get_or_create(email_id, **_params))
                except Exception, e:
                    print e
                    print "Erro ao tentar savar email seguimentado na lista"

        return emails_criados

    @classmethod
    def save_emails_mult(self, **params):
        """
        Este método pode receber uma lista de e-mails
        para melhorar os métodos de performance
        https://cloud.google.com/appengine/docs/standard/python/ndb/async
        """
        emails_criados = []
        for email in params['emails']:
            email_registrado = self.query(EmailLista.email == email['email'],
                                          EmailLista.lista == params['lista'].key).fetch()
            if not email_registrado:
                try:
                    email['lista'] = params['lista'].key,
                    _params = self.transform_input_data(email)

                    _em = EmailLista()
                    _em.lista = params['lista'].key
                    _em.nome = _params['nome']
                    _em.email = _params['email']
                    _em.sexo = _params['sexo'] if 'sexo' in _params else None
                    _em.extra = _params['extra'] if 'extra' in _params else None

                    emails_criados.append(_em)

                except Exception, e:
                    print e
                    print "Dados incompletos"

        return emails_criados


    @classmethod
    def get_email(self, email=None, _json=None):
        return self.query(EmailLista.email == email).fetch()

    @classmethod
    def get_email_by_list(self, lista_key=None, email=None):
        return self.query(EmailLista.lista == lista_key, EmailLista.email == email).get()

    @classmethod
    def get_emails_by_list(self, lista_key, projection=None, keys_only=None):
        if projection:
            return self.query(EmailLista.lista == lista_key).fetch(projection=projection)
        elif keys_only:
            return self.query(EmailLista.lista == lista_key).fetch(keys_only=True, limit=1000)
        else:
            return self.query(EmailLista.lista == lista_key).fetch()

    @classmethod
    def get_emails_by_list_async(self, lista_key):
        return self.query(EmailLista.lista == lista_key).fetch_async()

    @classmethod
    def get_count_by_list(self, lista_key):
        return self.query(EmailLista.lista == lista_key).count_async()

    @classmethod
    def get_count_by_loja_id(self, loja_id):
        listas_loja = Lista.get_lists_by_loja_id(str(loja_id))
        total = 0
        for i in listas_loja:
            total += self.query(EmailLista.lista == i.key).count_async()

        return total.get_result()

    @classmethod
    def get_paginated_itens(self, lista_key, cursor, type=None, size=10):
        cursor = ndb.Cursor(urlsafe=cursor)

        q = EmailLista.query(EmailLista.lista == lista_key).order(EmailLista.criado_em)
        items, next_curs, more = q.fetch_page(size, start_cursor=cursor)
        if more:
            next_c = next_curs.urlsafe()
        else:
            next_c = None

        data = None
        if type:
            data = {}
            data['emails'] = json.dumps([i.to_dict() for i in items], default=utils.datetime_to_json)
            data['cursor'] = next_c
        else:
            data = {'emails': items, 'cursor': next_c}

        return data

    @classmethod
    def cursor_pagination(cls, lista_key, prev_cursor_str, next_cursor_str, size=10):

        if not prev_cursor_str and not next_cursor_str:
            objects, next_cursor, more = EmailLista.query(EmailLista.lista == lista_key).order(EmailLista.criado_em).fetch_page(size)
            prev_cursor_str = ''
            if next_cursor:
                next_cursor_str = next_cursor.urlsafe()
            else:
                next_cursor_str = ''
            next_ = True if more else False
            prev = False
        elif next_cursor_str:
            cursor = ndb.Cursor(urlsafe=next_cursor_str)
            objects, next_cursor, more = EmailLista.query(EmailLista.lista == lista_key).order(EmailLista.criado_em).fetch_page(size, start_cursor=cursor)
            prev_cursor_str = next_cursor_str
            next_cursor_str = next_cursor.urlsafe()
            prev = True
            next_ = True if more else False
        elif prev_cursor_str:
            cursor = ndb.Cursor(urlsafe=prev_cursor_str)
            objects, next_cursor, more = EmailLista.query(EmailLista.lista == lista_key).order(-EmailLista.criado_em).fetch_page(size, start_cursor=cursor)
            objects.reverse()
            next_cursor_str = prev_cursor_str
            if next_cursor:
                prev_cursor_str = next_cursor.urlsafe()
            else:
                prev_cursor_str = ''
            prev = True if more else False
            next_ = True

        return {'objects': objects, 'next_cursor': next_cursor_str, 'prev_cursor': prev_cursor_str, 'prev': prev,
            'next': next_}


    @classmethod
    def reverse_pagination(self, lista_key, prev_cursor, next_cursor, back=False):
        # Set up.
        q = EmailLista.query(EmailLista.lista == lista_key)
        q_reverse = q.order(-EmailLista.criado_em)

        if not next_cursor and not prev_cursor:
            cursor = ndb.Cursor(urlsafe=None)
            objects, r_cursor, r_more = q_reverse.fetch_page(10, start_cursor=cursor)

            if r_cursor:
                next_cursor = r_cursor.urlsafe()
            else:
                next_cursor = ''
            _next = True if r_more else False
            prev = False
        elif next_cursor and back is False:
            cursor = ndb.Cursor(urlsafe=next_cursor)
            objects, r_cursor, r_more = q_reverse.fetch_page(10, start_cursor=cursor)
            prev_cursor = next_cursor
            next_cursor = r_cursor.urlsafe()
            _next = True if r_more else False
            prev = False
        elif prev_cursor:
            q_reverse = q.order(EmailLista.criado_em)
            cursor = ndb.Cursor(urlsafe=prev_cursor)
            objects, r_cursor, r_more = q_reverse.fetch_page(10, start_cursor=cursor)
            objects.reverse()
            next_cursor = prev_cursor
            if r_cursor:
                prev_cursor = r_cursor.urlsafe()
            else:
                prev_cursor = ''
            prev = True if r_more else False
            _next = True


        return {'objects': objects, 'next_cursor': next_cursor, 'prev_cursor': prev_cursor, 'prev': prev,
                'next': _next}

    @classmethod
    def get_count_by_loja_id_from_to(cls, loja_id, data_ini, data_fim):
        listas_loja = Lista.get_lists_by_loja_id(str(loja_id))
        _emails = []
        for i in listas_loja.get_result():
            _emails.append(EmailLista.query(EmailLista.lista == i.key,
                         EmailLista.criado_em >= data_ini,
                         EmailLista.criado_em <= data_fim
            ).order(cls.criado_em).fetch(5))


        return _emails


class Desistentes(BaseModel):
    """
    Classe que armazena todos os pedidos de cancelamento de inscrição
    ao receber as ofertas da Tad Target em relação a Loja X.
    """
    loja_id = ndb.StringProperty(required=True)
    email = ndb.StringProperty(required=True)

class Lista(BaseModel):
    """
    Cria uma lista de e-mail pronta para seguimentação. Define o nome da lista
    em loja e a quantidade de e-mails que esta lista possúi.
    """
    nome = ndb.StringProperty(required=True)
    loja_id = ndb.StringProperty(required=True)
    #todo: adicionar como manipular o ID da lista quando for execudada dentro do mail chim
    #todo: também verifiquei que os métodos de armazenamento são um pouco velhos, melhorar.
    mailchimp_id = ndb.StringProperty()
    mailchimp_batches = ndb.StringProperty(repeated=True)

    @classmethod
    def get_list(self, nome=None, loja_id=None):
        return self.query(Lista.nome == nome, Lista.loja_id == loja_id).get()

    @classmethod
    def save_list(self, params=None):
        # se não existir a lista cria uma
        lista = self.get_list(params['nome'], params['loja_id'])
        if not lista:
            try:
                lista_id = "lista-%s" % str(randint(0,9999999999999))
                lista = self.get_or_create(lista_id, **params)
            except Exception, e:
                print e
                print "erro ao tentar salvar lista de e-mail seguimentada"

        if isinstance(lista, tuple):
            lista = lista[0]
        return lista


    @classmethod
    def get_lists_by_loja_id(self, loja_id=None):
        return self.query(Lista.loja_id == loja_id).fetch_async()



