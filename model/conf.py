#encoding: utf-8
from google.appengine.ext.blobstore import blobstore
from google.appengine.ext.ndb import BlobKey

__author__ = 'tadgaudio@gmail.com (Tadeu luis Pires Gaudio)'

from google.appengine.ext import ndb
from libs.basemodel import BaseModel
import itertools

class Configuracoes(BaseModel):
    user_id = ndb.StringProperty(required=True)
    nome_loja = ndb.StringProperty(required=True)
    url_loja = ndb.StringProperty()
    url_logo = ndb.StringProperty()
    analytics_account_id = ndb.StringProperty()
    analytics_property_id = ndb.StringProperty()
    analytics_view_id = ndb.StringProperty()
    merchant_url = ndb.StringProperty()
    nome_remetente = ndb.StringProperty()
    xml_blob_key = ndb.StringProperty()
    xml_fetching = ndb.BooleanProperty(default=False)
    xml_itens_enviados = ndb.IntegerProperty()
    xml_itens_ativos = ndb.IntegerProperty()
    assunto = ndb.StringProperty()


    @classmethod
    def save_conf(self, **params):
        _params = self.transform_input_data(params)
        _params_endereco = Endereco.transform_input_data(params)
        _params_telefone = Telefone.transform_input_data(params)
        _params_empresa = Empresa.transform_input_data(params)
        _params_sender = Senders.transform_input_data(params)
        configuracao = None
        erro = None
        try:
            configuracao = self.get_or_create(params['user_id'], **_params)
            configuracao[0].populate(**_params)
            configuracao[0].put()

            endereco = Endereco.get_or_create(params['user_id'], **_params_endereco)
            endereco[0].populate(**_params_endereco)
            endereco[0].put()

            telefone = Telefone.get_or_create(params['user_id'], **_params_telefone)
            telefone[0].populate(**_params_telefone)
            telefone[0].put()

            empresa = Empresa.get_or_create(params['user_id'], **_params_empresa)
            empresa[0].populate(**_params_empresa)
            empresa[0].put()

            senders = Senders.get_or_create(params['user_id'], **_params_sender)
            senders[0].populate(**_params_sender)
            senders[0].put()

        except Exception, e:
            print e
            erro = e

        return configuracao, erro

    @classmethod
    def get_conf(self, user_id=None):
        return self.query(Configuracoes.user_id == user_id).fetch()

    @classmethod
    def get_all_active_stores(self):
        return self.query().fetch()



class Empresa(BaseModel):
    cnpj = ndb.StringProperty()
    ie = ndb.StringProperty()
    razao_social = ndb.StringProperty()
    url_logo = ndb.StringProperty()
    facebook = ndb.StringProperty()
    mensagem = ndb.TextProperty()

class Endereco(BaseModel):
    cep = ndb.StringProperty()
    endereco = ndb.StringProperty()
    numero = ndb.StringProperty()
    complemento = ndb.StringProperty()
    bairro = ndb.StringProperty()
    pais = ndb.StringProperty()
    cidade = ndb.StringProperty()
    estado = ndb.StringProperty()

class Telefone(BaseModel):
    telefone = ndb.StringProperty()
    celular = ndb.StringProperty()
    whatsapp = ndb.StringProperty()



class ProdutosLoja(BaseModel):
    """
    Esta classe abriga diferêntes versões de XML das lojas a fim de agilizar o processo de envio e apresentação
    dos produtos nas diversas listagens das vitrines.
    """
    loja_id = ndb.IntegerProperty(required=True)
    google_merchant_feed = ndb.BlobKeyProperty(required=True)
    #todo: tem que implementar a leitura correta do Feed do Google Merchant após upload
    #como não implementei a parte do upload direto do feed ele traz um qualquer o que dá erro.

    @classmethod
    def get_by_loja_id(cls, loja_id):
        return cls.query(ProdutosLoja.loja_id == loja_id).fetch()

    @classmethod
    def save_google_merchant_feed(cls, xml, loja_id):
        try:
            sim = ProdutosLoja(
                parent=ndb.Key("ProdutosLoja", str(loja_id) or "*noid*"),
                loja_id=loja_id,
                google_merchant_feed=xml
            ).put()
            return sim
        except Exception, e:
            print "erro ao tentar gravar %s" % cls
            print e

        return None



class Senders(BaseModel):
    """
    Modelo de dados que irá abrigar os senders dos e-mails que são disparados
    por nossos clientes, inicialmente esse sender é feito via sendgrid.
    para cadastra as coisas no sendgrid ele pede também obrigatóriamente
    endereço e pais, porém estas informações na nossa plataforma vem do cadastro minha conta.
    """
    loja_id = ndb.StringProperty()
    from_name = ndb.StringProperty()
    from_email = ndb.StringProperty()
    reply_to = ndb.StringProperty()
    nickname = ndb.StringProperty()
    verified = ndb.BooleanProperty(default=False)
    loked = ndb.BooleanProperty(default=True)
    sender_id = ndb.StringProperty()

    @classmethod
    def get_by_loja_id(cls, loja_id):
        return cls.query(Senders.loja_id == loja_id).fetch()


class AutoEmail(BaseModel):
    """
    Modelo de dados que irá abrigar os horários que podem
    ser disparados os e-mails marketing
    """
    loja_id = ndb.StringProperty()
    dias = ndb.StringProperty(repeated=True)
    horas = ndb.StringProperty(repeated=True)
    template = ndb.KeyProperty(kind='EmailTemplate')
    listas = ndb.KeyProperty(kind='Lista', repeated=True)
    ativa = ndb.BooleanProperty(default=True)
    timezone = ndb.StringProperty(default="America/Sao_Paulo")

    @classmethod
    def get_frequencia(self, loja_id):
        return self.query(AutoEmail.loja_id == str(loja_id)).get()

    @classmethod
    def get_frequencias(self, loja_id):
        return self.query(AutoEmail.loja_id == str(loja_id))

    @classmethod
    def save_frequencia(cls, **params):

        _listas = []
        _listas_bkp = params['listas'] #crio um backup pois ao sobrescrever itens já existente as listas tem que ser
                                       #com as chaves em string
        #if len(params['listas']) > 1:
        for lista in params['listas']:
            _listas.append(ndb.Key('Lista', lista))

        params['listas'] = _listas
        params['template'] = ndb.Key('EmailTemplate', long(params['template']))

        sss = "".join(params['dias'])+"".join(params['horas'])

        freq, created = cls.get_or_create(params['loja_id']+sss, **params)

        if not created:
            try:
                params['listas'] = _listas_bkp
                freq.populate(**params)
                freq.put()
            except Exception, e:
                print "Erro ao tentar cadastrar a frequência"
                print e

        return freq

    @classmethod
    def apagar_programacao(cls, id):
        """
        Remove atividade de disparo automático do E-mail Marketing
        :param id: id da programação
        :return: True or False
        """
        return cls.get_by_id(id).key.delete()

    @classmethod
    def pausar_programacao(cls, id):
        """
        Inativa os envios da progração em execução

        :param id: id da programação
        :return:  True or False
        """
        programacao = cls.get_by_id(id)
        programacao.ativa = False
        programacao.put()
        return programacao

    @classmethod
    def ativar_programacao(cls, id):
        """
        Ativa os envios da progração atualmente pausada

        :param id: id da programação
        :return:  True or False
        """
        programacao = cls.get_by_id(id).get()
        programacao.ativa = True
        programacao.put()
        return programacao

class ConfiguracoesCarrinhos(BaseModel):
    """
    Modelo de dados que irá abrigar as configurações extras
    para o disparo dos carrinhos abandonados. Por padrão elas não usam nada
    apenas disparam confome 20minutos após ter recebido o carrinho.
    """
    loja_id = ndb.StringProperty(required=True)
    frequencia = ndb.StringProperty()
    abandon_time = ndb.StringProperty()
    cupom_1 = ndb.StringProperty()
    cupom_2 = ndb.StringProperty()

    @classmethod
    def get_config(self, loja_id):
        return self.query(ConfiguracoesCarrinhos.loja_id == str(loja_id)).get()

    @classmethod
    def save_conf(cls, **params):
        confs = cls.get_or_create(params['loja_id'], **params)
        try:
            confs[0].populate(**params)
            confs[0].put()
        except Exception, e:
            print "Erro ao tentar cadastrar as configurações extras para os carrinhos abandonados"
            print e

        return confs


class SetupGoals(BaseModel):
    loja_id = ndb.StringProperty(required=True)
    time = ndb.StringProperty(required=True)
    nusers = ndb.StringProperty()
    marketing_costs = ndb.StringProperty()
    product_costs = ndb.StringProperty()
    company_employers = ndb.StringProperty()
    company_tax_fee = ndb.StringProperty()

    @classmethod
    def save_goals(self, **params):
        _params = self.transform_input_data(params)
        configuracao = None
        erro = None
        try:
            configuracao = self.get_or_create(params['loja_id'], **_params)
            configuracao[0].populate(**_params)
            configuracao[0].put()
        except Exception, e:
            print e
            erro = e

        return configuracao, erro

    @classmethod
    def get_by_loja_id(self, loja_id=None):
        return self.query(SetupGoals.loja_id == loja_id).get()


class Payment(BaseModel):
    loja_id = ndb.StringProperty(required=True)
    payment_type = ndb.StringProperty()
    payment_received = ndb.StringProperty()
    payment_sended = ndb.StringProperty()
    payment_processing = ndb.StringProperty()

    @classmethod
    def insert_payment(self, **params):
        _params = self.transform_input_data(params)
        configuracao = None
        erro = None
        try:
            configuracao = self.get_or_create(params['loja_id'], **_params)
            configuracao[0].populate(**_params)
            configuracao[0].put()
        except Exception, e:
            print e
            erro = e

        return configuracao, erro

    @classmethod
    def get_payments(self, loja_id=None):
        return self.query(Payment.loja_id == loja_id).fetch()

