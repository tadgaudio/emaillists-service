#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Tadeu Luis Pires Gaudio <tadgaudio@gmail.com>'
"""
Aqui temos Handlers e Métodos para se trabalhar com a integração do Google Merchant Feed
"""
import webapp2
from google.appengine.api import urlfetch
import libs.cloudstorage as gcs
from model import conf
#from libs.handlers import perfil
from libs.decorators import taskqueue_method, cron_method

# Retry can help overcome transient urlfetch or GCS issues, such as timeouts.
my_default_retry_params = gcs.RetryParams(initial_delay=0.2,
                                          max_delay=5.0,
                                          backoff_factor=2,
                                          max_retry_period=15)
# All requests to GCS using the GCS client within current GAE request and
# current thread will use this retry params as default. If a default is not
# set via this mechanism, the library's built-in default will be used.
# Any GCS client function can also be given a more specific retry params
# that overrides the default.
# Note: the built-in default is good enough for most cases. We override
# retry_params here only for demo purposes.
gcs.set_default_retry_params(my_default_retry_params)


tmp_filenames_to_clean_up = []

# When writing a file to Cloud Storage, you should not call finally:close() as
# this can result in finalizing object uploads even if there was an exception
# during a write.
def create_file(filename, content, content_type="text/xml"):
    """Create a file.

    The retry_params specified in the open call will override the default
    retry params for this particular file handle.

    Args:
      filename: filename.
    """
    write_retry_params = gcs.RetryParams(backoff_factor=1.1)
    gcs_file = gcs.open(filename,
                        'w',
                        content_type=content_type,
                        # options={'x-goog-meta-foo': 'foo',
                        #          'x-goog-meta-bar': 'bar'},
                        retry_params=write_retry_params)
    gcs_file.write(content)
    gcs_file.close()
    tmp_filenames_to_clean_up.append(filename)


def read_file(filename):
    conteudo = None
    try:
         gcs_file = gcs.open(filename)
         conteudo = gcs_file.read()
         gcs_file.close()
    except Exception, e:
        print e

    # linha_inicial = gcs_file.readline() faz ler 1000 bytes 1k do arquivo caso queria apresentar
    #linha_final = gcs_file.seek(-1024, os.SEEK_END)
    return conteudo


def stat_file(filename):
    print 'File stat:\n'
    stat = gcs.stat(filename)
    return repr(stat)


class DownloadMerchantFeed(webapp2.RequestHandler):
    """
    Faz o download do Google Merchants quando na URL do site.
    """
    @cron_method
    def get(self):
        import libs.handlers.vitrines as vt
        bucket_name = 'tadtarget-xml'

        lojas = conf.Configuracoes.get_all_active_stores()
        for loja in lojas:
            result = None
            if loja.merchant_url != 'None' and loja.merchant_url is not None:
                #todo: não sei porque mas aqui está gastando muita memória para executar essa ação.
                try:
                    print "Download Feed: %s" % loja.merchant_url
                    result = urlfetch.fetch(loja.merchant_url, deadline=60, method=urlfetch.GET).content
                except Exception, e:
                    print "Exception na hora de baixar o arquivo"
                    print loja.merchant_url
                    print e

            if result:
                bucket = '/' + bucket_name
                filename = bucket + '/'+str(loja.key.id())+'-shopping'
                create_file(filename, result)
                #todo: aqui é preciso reduziar a quantidade de PUT e GET tentar usando put_multi, get_multi.
                vt.prefetch_vitrines(loja.key.id(), loja.analytics_view_id)


class DownloadPainelResults(webapp2.RequestHandler):
    """
    Faz o download do Google Merchants quando na URL do site.
    """
    @cron_method
    def get(self):
        import libs.handlers.vitrines as vt
        print "vai iniciar o processo de download dos resultados da loja"
        lojas = conf.Configuracoes.get_all_active_stores()
        for loja in lojas:
            try:
                print " vai fazer donwload dos resultaos %s" % loja.key.id()
                vt.prefetch_results(loja.key.id(), loja.analytics_view_id)
            except Exception, e:
                print "Erro ao tentar fazer prefetch dos resultados para o painel do Cliente %s" % loja.key.id()
                print e


