#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

config = {

    # This config file is used only in appengine.beecoss.com (Sample website)
    # Don't use values defined here
    'app_name': "TadTarget - Big Data & Marketing",
    'webapp2_extras.sessions': {'secret_key': '3v3rTh1ngsMyf4ultw:+9u3}nlrf8U?'},
    # webapp2 authentication
    'webapp2_extras.auth': {
      'user_model': 'bp_models.User',
      'user_attributes': ['email_address'] # isso cacheia alguns atributos do usuário na sessão
    },

    # jinja2 templates
    # 'webapp2_extras.jinja2': {'template_path': ['tpl', 'tpl/emails']},
                              # 'environment_args': {'extensions': ['jinja2.ext.i18n']}}, #só adicionar isso se tiver a lib babel instalada.

    'environment': "localhost",

    # contact page email settings
    'contact_sender': "ofertas@tadtarget.com",
    'contact_recipient': "tadgaudio@gmail.com",
    'suport_email': 'ofertas@tadtarget.com',

    'send_mail_developer': False,

    # fellas' list
    'developers': (
        ('GAE Developer', 'tadgaudio@gmail.com'),
    ),

    #Google API Access Configuration
    'CLIENT_SECRETS': os.path.join(os.path.dirname(__file__), 'client_secret.json'),
    'CLIENT_SECRETS_SERVER': os.path.join(os.path.dirname(__file__), 'tadtarget-adf5b4eec954.json'),
    'GDATA_URL_PERMISSION': {
        'analytics':'https://www.googleapis.com/auth/analytics.readonly'
    },
    # It is just an example to fill out this value
    'google_tagmanager_code_head': """<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TJ67P88');</script>""",

# It is just an example to fill out this value
    'google_tagmanager_code_body': """<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TJ67P88"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>""",

    # Password AES Encryption Parameters
    # aes_key must be only 16 (*AES-128*), 24 (*AES-192*), or 32 (*AES-256*) bytes (characters) long.
    'aes_key': "9c20576a4330bbe719b23ac8bf3bb8a1",
    'salt': "RdbkETeF$<^>%%X^8|e[9td62`dobFL[V&F&**@`UP6vqjGL,>v+k@ma^zd6WdG0;H>o-SGG9ynk",
    # get your own consumer key and consumer secret by registering at https://dev.twitter.com/apps
    # callback url must be: http://[YOUR DOMAIN]/login/twitter/complete
    #'twitter_consumer_key': 'rrgGr00w3HRJYzmxNLMgzQ',
    #'twitter_consumer_secret': '5IdJDvSRdbkETeFjFeIxS7IoERWn8nGk5NfGSGG9ynk',

    #Facebook Login
    # get your own consumer key and consumer secret by registering at https://developers.facebook.com/apps
    #Very Important: set the site_url= your domain in the application settings in the facebook app settings page
    # callback url must be: http://[YOUR DOMAIN]/login/facebook/complete
    #'fb_api_key': '265551733563729',
    #'fb_secret': '9de58570269f23b768726f3617ceb6ce',

    #Linkedin Login
    #Get you own api key and secret from https://www.linkedin.com/secure/developer
    #'linkedin_api': 'ueNRJIsyU3Q_EXer9MTOT3fpH-rQCGZWBBhVCeV3gyDzgNSB9Ov04DM3j6WEpSHf',
    #'linkedin_secret': 'NYgmelU0_7PKf0LXYNq8ujtrp0F9UWBKaxd1hQOoBwiwVecHyZB9uTihZ-y7g4Me',
    #AWS S3

    #nova conta da AWS
    #'aws-s3-url': 'https://tt-ig.s3-website-sa-east-1.amazonaws.com/',
    'aws-s3-url': 'https://tt-ig.s3.amazonaws.com/',
    'aws-access-key-id':'AKIAJLK6RBUEGEX6IITQ',
    'aws-secret-access-key':'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',

    #acesso antigo
    #'aws-s3-url': 'https://esportebase-aws.s3.amazonaws.com/',
    #'aws-access-key-id':'AKIAJB22EAIOT7N2ZYLA',
    #'aws-secret-access-key':'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',

    #sendgrid APIKey
    'SENDGRID_API_KEY': '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',
    # Github login
    # Register apps here: https://github.com/settings/applications/new
    #'github_server': 'github.com',
    #'github_redirect_uri': 'http://appengine.beecoss.com/social_login/github/complete',
    #'github_client_id': 'c391bcae77f69499bb46',
    #'github_client_secret': '00674550b11a7644e675129a4ddbdaa2798addae',

    # get your own recaptcha keys by registering at http://www.google.com/recaptcha/
    'captcha_public_key_v1': "6Ldi0u4SAAAAAC8pjDop1aDdmeiVrUOU2M4i23tT",
    'captcha_private_key_v1': "6Ldi0u4SAAAAAPzk1gaFDRQgry7XW4VBvNCqCHuJ",

    'captcha_public_key': "6LdfJiAUAAAAAOkTlWD3HyoOjf-8ox9q2tSdjkXA",
    'captcha_private_key': "6LdfJiAUAAAAAOPmyC8RNQRLAUd25VRsiAC4Q4oq",


    # ----> ADD MORE CONFIGURATION OPTIONS HERE <----

    #google app-in payments
    'seller_identifier': '12919778635379614757',
    'seller_secret':'T62F4q_uv8VTdyjnsMSoQQ',
    'seller_secret_dev':'qrMUl3biH1ld-g4A4ofb4g',

    #paypal payments
    'paypal_client_id': 'AdOHKJ8I10-A-5FHBODi1_168-1bkLJm5XE9XPZ1LMBbALoySriDXZlDrz2ehtknQwPk8QOKX0zPJUP5',
    'paypal_secret': 'EPpWQISWBRVraTG-dBLHu8GkwyF_Fmuw6H48prgdvnKJwl5GoV15FpocOI7Qroi3JjykXpV9S7sytB94',

    'devOps': {
        'url': 'http://localhost:8888',
        'localhost_url': 'http://localhost:8888',
        'deploy_url': 'https://app.tadtarget.com',
        'app_engine_url': 'http://tad-target-1985.appspot.com'
    },

    'ipBlock': ['http://www.gbabystore.com.br', 'http://www.italastore.com.br'],

    'FEEDBACK_PER_PAGE': 20,
    'TADTARGET_COUNTER': 'TADTARGET',
    'DEFAULT_COUNTER_NAME': 'AUTOEMAIL',
    'DEFAULT_COUNTER_CART': 'CARRINHO'

}