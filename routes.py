#!/usr/bin/env python
# coding=utf-8

__author__ = 'tadgaudio@gmail.com (Tadeu luis Pires Gaudio)'

"""
Using redirect route instead of simple routes since it supports strict_slash
Simple route: http://webapp-improved.appspot.com/guide/routing.html#simple-routes
RedirectRoute: http://webapp-improved.appspot.com/api/webapp2_extras/routes.html#webapp2_extras.routes.RedirectRoute
LazyHandlers: https://webapp-improved.appspot.com/guide/routing.html#lazy-handlers
"""

from webapp2_extras.routes import RedirectRoute
from webapp2_extras.routes import PathPrefixRoute

secure_scheme = 'https'

#====== system routes =======#
_routes = [

    # urls do usuário
    PathPrefixRoute('/emaillists/task', [

        RedirectRoute('/', 'main.MainHandler', name='main-handler', strict_slash=True),
        RedirectRoute('/process-email-lists', 'processor.ProcessEmailLists',
                      name='process-email-list', strict_slash=True),

    ]),
]


def get_routes():
    return _routes

def add_routes(app):
    if app.debug:
        secure_scheme = 'http'
    for r in _routes:
        app.router.add(r)