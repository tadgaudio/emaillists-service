#!/usr/bin/env python
# coding=utf-8
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import csv
from random import randint

from model.listas import EmailLista, Lista
from integrador.google_shopping import read_file
from libs.decorators import taskqueue_method
from google.appengine.api import mail
from google.appengine.ext import ndb

from libs import utils


def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(utf_8_encoder(unicode_csv_data),
                            dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield [unicode(cell, 'utf-8') for cell in row]


def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        try:
            yield line.encode('utf-8')
        except Exception, e:
            print "yeild line: {}".format(e)
            yield line


def get_emails_from_csv_file(ffile):
    """
    Recebe o arquivo de lista de e-mail retorna um objeto com os e-mails a serem salvos.
    @return:
    """
    import re
    EMAIL_REGEX = re.compile(r"^[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+.[a-zA-Z]{2,6}$")
    valid_adresses = []

    # todo: não sei porque tem isso?
    # if 'value' in ffile:
    #    reader = csv.reader(ffile.value.split("\n"))

    reader = ''
    if ffile:
        reader = unicode_csv_reader(ffile.split("\n"))
    _tm = 0

    for row in reader:
        _tm += 1

        if len(row) > 2:

            if EMAIL_REGEX.match(row[1]):
                _customer = {
                    'nome': utils.parse_ecode_br(row[0]),
                    'email': row[1],
                    'sexo': row[2]
                }

                # todo: impelmentar aqui uma forma de traduzir o cabeçalho para informações extras
                if len(row) >= 3:
                    _ex = {}
                    for i in range(3, len(row)):
                        _ex['_info%s' % i] = row[i]
                    _customer['extra'] = _ex

                valid_adresses.append(_customer)

        elif len(row) == 2:
            # lista simples sem sexo geralmente nome e email
            if EMAIL_REGEX.match(row[1]):
                valid_adresses.append({
                    'nome': row[0],
                    'email': row[1]
                })

    return valid_adresses


class ProcessEmailLists(webapp2.RequestHandler):
    """
    Core Handler for processing a big E-mail Contact List
    Use with TaskQueue
    todo: melhorar velocidade
    Para melhorar a velocidade de salvar as coisas no banco podemos utilizar algo no sentido abaixo
    https://stackoverflow.com/questions/21339536/defer-multiple-tasks-in-google-app-engine-asynchronously
    """

    @taskqueue_method
    def post(self):
        ffile = self.request.get("file")
        filename = self.request.get("filename")
        nome = self.request.get("nome")
        lista_id = self.request.get("lista_id")
        loja_id = self.request.get("loja_id")
        email = self.request.get("email_aviso")
        message = "Contatos processados"

        # print "Arquivo em processamento: {}".format(nome.encode('utf-8'))
        if len(ffile) > 20048 or filename:
            # print "Arquivo grande, vamos pegar ele no Clooud Storage {}".format(filename)
            ffile = read_file(filename)

        valid_adress = get_emails_from_csv_file(ffile)

        if not lista_id:
            _lista = Lista.get_list(nome=nome, loja_id=loja_id)
            if not _lista:
                lista_id = "lista-%s" % str(randint(0, 9999999999999))
            else:
                lista_id = _lista.key.id()

        lista = Lista.get_or_create(lista_id, **{'nome': nome, 'loja_id': loja_id})
        # print "Lista: Criada/Localizada: {} Loja {}".format(nome.encode('utf-8'), loja_id)

        if lista:
            saved_emails = None
            if len(valid_adress) > 0:
                # todo: esse processo pode demorar muito o certo é colocar em uma taskqueue usar as dicas abaixo
                # https://cloud.google.com/appengine/docs/standard/python/ndb/async
                saved_emails = EmailLista.save_emails_mult(**{
                    'lista': lista[0],
                    'emails': valid_adress
                })

            _batch_size = 100

            _mult_mails = [saved_emails[i:i + _batch_size] for i in range(0, len(saved_emails), _batch_size)]

            for lote in _mult_mails:
                ndb.put_multi(lote)

            if saved_emails:
                if len(saved_emails) > 0:
                    message = "Deu certo! E-mails salvos! %s" % len(saved_emails)
                elif len(saved_emails) == 0:
                    message = "Os e-mails dessa lista já existem"
                else:
                    message = "Problema ao salvar emails da lista, verificar arquivo"

            try:
                assunto = "Arquivo de E-mails processado com sucesso!"
                # print "Envia e-mail ao cliente avisando sobre o carregamento da lista"

                _email = mail.EmailMessage()
                _email.sender = 'ofertas@tadtarget.com'
                _email.to = "%s <%s>" % (
                'Tad Target - Big Data & Analytics'.decode('utf-8'), str(email))  # 'tadeu.gaudio@meusalao.com.br'
                _email.subject = assunto
                _email.html = message
                _email.send()
            except Exception, e:
                print "Error sending email: %s" % e
                self.response.out.write('error')
