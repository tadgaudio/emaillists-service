#encoding: utf-8

import cgi
from google.appengine.ext import ndb
from webapp2 import uri_for
from utils import slugify
import json

class BaseModel(ndb.Model):
    criado_em = ndb.DateTimeProperty(auto_now_add=True)
    modificado_em = ndb.DateTimeProperty(auto_now=True)

    @classmethod
    def basemodel_query(cls, ancestor_key):
        return cls.query(ancestor=ancestor_key).order(-cls.criado_em)

    @classmethod
    def get_key_name(cls, key_name):
        """
        Metodo remove espacos em branco excedentes e minimiza a string,
        para criar uma key
        """
        return slugify(key_name)

    @classmethod
    def get_by_key_name(cls, key_name):
        return ndb.Key(cls, key_name).get()

    # noinspection PyProtectedMember
    @classmethod
    def transform_input_data(cls, kwargs):
        """
        Transforma valores dos campos recebidos em formatos utilizados pelo modelo
        além de remover do dicionário campos vazios
        - key_id -> KeyObj
        - cgi.FieldStorage -> byte string
        """
        d = {}
        for k, v in kwargs.iteritems():
            if hasattr(cls, k):
                field = getattr(cls, k)
                if v is not None and not v == '' and not v == []:
                    if isinstance(field, ndb.KeyProperty) and (isinstance(v, basestring) or isinstance(v, list)):
                        if field._repeated:
                            d[k] = [ndb.Key(field._kind, i) for i in v]
                        else:
                            kind = field._kind if field._kind else field._name
                            d[k] = ndb.Key(kind, v)
                    elif isinstance(v, cgi.FieldStorage) and isinstance(field, ndb.BlobProperty):
                        d[k] = v.value
                    else:
                        d[k] = v
        return d



    # noinspection PyArgumentList
    @classmethod
    @ndb.transactional
    def get_or_create(cls, key_string, **kwargs):
        """
        Adiciona get_or_create ao modelo retornando também
        uma flag indicando se objeto foi criado ou não.

        Baseado em http://stackoverflow.com/a/14549493/1272513 (por Guido)

        Update: o Leandro fez isso porque o método get_or_insert do NDB é trivial
        e não retorna se o objeto foi criado ou já existia. Então é a versão mais top
        do get_or_insert
        """
        key = ndb.Key(cls, cls.get_key_name(key_string))
        ent = key.get()
        if ent is not None:
            return ent, False  # False meaning "not created"
        ent = cls(**kwargs)
        ent.key = key
        ent.put()
        return ent, True  # True meaning "created"

    @classmethod
    def get_choices(cls, field_name, _filter=None):
        """
        O método irá trazer as informações que estiverem dentro
         dos parâmetros que foram passados, o filtro pode ser usado para buscar
         em uma entidade já definida.
        :param field_name: o nome do campo que queremos trazer no banco de dados
        :param _filter: uma lista de filtros para acompanhar a pesquisa Ex Modelo.filter(Modelo.key.
        :return: retorna uma lista de tuple com os resultados encontrados
        """
        def get_choice_tuple(obj):
            return obj.key.id(), getattr(obj, field_name)

        q = cls.query().order(getattr(cls, field_name))
        if _filter is None:
            _filter = []
        elif type(_filter) != list:
            _filter = [_filter]
        for f in _filter:
            q = q.filter(getattr(cls, f['field_name']) == f['field_value'])
        return q.map(get_choice_tuple)

    def populate(self, **kwds):
        kwds = self.transform_input_data(kwds)
        return super(BaseModel, self).populate(**kwds)

    def export(self, exclude=None, **kwargs):
        d = {}
        for k, v in self.to_dict(exclude=exclude).iteritems():
            if v:
                final = None
                if isinstance(v, ndb.Key):
                    final = v.get().export(exclude=exclude, **kwargs)
                elif isinstance(v, list) and isinstance(v[0], ndb.Key):
                    final = [i.get().export(exclude=exclude, **kwargs) for i in v]
                elif type(self._properties[k]) == ndb.BlobProperty:
                    final = self.get_image_url(k, **kwargs)
                else:
                    final = unicode(v)
                if final:
                    d[k] = final
        if d:
            d.update({'id': self.key.id()})
        else:
            d = None
        return d

    def get_json(self, exclude=None):
        return json.dumps(self.export(exclude))

    def get_image_url(self, field_name, **kwargs):
        host_part = 'http://' + kwargs.get('host', 'www.esportebase.com.br') if kwargs.get('with_host') else ''
        return host_part + uri_for('api-img-id', field=field_name) + '?id=' + self.key.urlsafe()

    @classmethod
    def get_query(cls, **kwargs):
        return cls.query()

    @classmethod
    def export_all(cls, exclude=None, **kwargs):
        # TODO: adicionar testes
        q = cls.get_query(**kwargs)
        return q.map(lambda obj: obj.export(exclude=exclude, **kwargs))

    @classmethod
    def get_all_json(cls, **kwargs):
        # TODO: adicionar testes
        return json.dumps(cls.export_all(**kwargs))