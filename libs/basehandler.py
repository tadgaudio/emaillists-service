# *-* coding: UTF-8 *-*

# standard library imports
import logging
import re
import os
# related third party imports
import webapp2
from webapp2_extras import jinja2
from webapp2_extras import auth
from webapp2_extras import sessions

# local application/library specific imports
import utils
import jinja_bootstrap
import bp_models as models
from model import conf
import config

import model.transactions as trans

from webapp2_extras import i18n

class ViewClass:
    """
        ViewClass to insert variables into the template.

        ViewClass is used in BaseHandler to promote variables automatically that can be used
        in jinja2 templates.
        Use case in a BaseHandler Class:
            self.view.var1 = "hello"
            self.view.array = [1, 2, 3]
            self.view.dict = dict(a="abc", b="bcd")
        Can be accessed in the template by just using the variables liek {{var1}} or {{dict.b}}
    """
    pass


class BaseHandler(webapp2.RequestHandler):
    """
        BaseHandler for all requests

        Holds the auth and session properties so they
        are reachable for all requests
    """

    def __init__(self, request, response):
        """ Override the initialiser in order to set the language.
        """
        self.initialize(request, response)
        self.view = ViewClass()


    def dispatch(self):
        """
            Get a session store for this request.
        """
        if self.request.referer in config.config["ipBlock"]:
            return self.response

        self.session_store = sessions.get_store(request=self.request)

        try:
            #csrf protection
            #adicione as páginas que não precisam da proteção
            page = re.match(r'/taskqueue-admin/|/bill|/popup', self.request.path)

            if self.request.method == "POST" and not page:
               if not page:
                   token = self.session.get('_csrf_token')
                   if not token or (token != self.request.get('_csrf_token') and
                            token != self.request.headers.get('_csrf_token')):
                       self.abort(403)

            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)


    @webapp2.cached_property
    def user_model(self):
        """Returns the implementation of the user model.

        Keep consistency when config['webapp2_extras.auth']['user_model'] is set.
        """
        return self.auth.store.user_model

    @webapp2.cached_property
    def auth(self):
        return auth.get_auth()

    @webapp2.cached_property
    def auth_config(self):
        """
              Dict to hold urls for login/logout
        """
        return {
            'login_url': self.uri_for('login'),
            'logout_url': self.uri_for('logout')
        }

    @webapp2.cached_property
    def user_info(self):
        return self.auth.get_user_by_session()


    @webapp2.cached_property
    def user_id(self):
        return str(self.user_info['user_id']) if self.user_info else None

    @webapp2.cached_property
    def user_key(self):
        if self.user_info:
            user_info = self.user_model.get_by_id(long(self.user_id))
            return user_info.key
        return None

    @webapp2.cached_property
    def username(self):
        if self.user_info:
            try:
                user_info = self.user_model.get_by_id(long(self.user_id))
                if not user_info.activated:
                    self.auth.unset_session()
                    self.redirect_to('home')
                else:
                    return str(user_info.username)
            except AttributeError, e:
                # avoid AttributeError when the session was delete from the server
                logging.error(e)
                self.auth.unset_session()
                self.redirect_to('home')
        return None

    @webapp2.cached_property
    def user_plan(self):
        plan = 'Free'
        if self.user_id:
            try:
                plan = trans.Plano.get_by_id(str(self.loja_id)).plan
            except AttributeError, e:
                # avoid AttributeError when the session was delete from the server
                logging.error(e)

        return plan


    @webapp2.cached_property
    def email(self):
        if self.user_info:
            try:
                user_info = self.user_model.get_by_id(long(self.user_id))
                return user_info.email
            except AttributeError, e:
                # avoid AttributeError when the session was delete from the server
                logging.error(e)
                self.auth.unset_session()
                self.redirect_to('home')
        return None


    @webapp2.cached_property
    def current_user(self):
        user = self.auth.get_user_by_session()
        if user:
            return self.user_model.get_by_id(user['user_id'])
        return None

    @webapp2.cached_property
    def provider_uris(self):
        login_urls = {}
        continue_url = self.request.get('continue_url')
        for provider in self.provider_info:
            if continue_url:
                login_url = self.uri_for("social-login", provider_name=provider, continue_url=continue_url)
            else:
                login_url = self.uri_for("social-login", provider_name=provider)
            login_urls[provider] = login_url
        return login_urls

    @webapp2.cached_property
    def provider_info(self):
        return models.SocialUser.PROVIDERS_INFO

    @webapp2.cached_property
    def is_mobile(self):
        return utils.set_device_cookie_and_return_bool(self)

    @webapp2.cached_property
    def jinja2(self):
        return jinja2.get_jinja2(factory=jinja_bootstrap.jinja2_factory, app=self.app)

    @webapp2.cached_property
    def get_base_layout(self):
        """
        Get the current base layout template for jinja2 templating. Uses the variable base_layout set in config
        or if there is a base_layout defined, use the base_layout.
        """
        return self.base_layout if hasattr(self, 'base_layout') else self.app.config.get('base_layout')

    def set_base_layout(self, layout):
        """
        Set the base_layout variable, thereby overwriting the default layout template name in config.py.
        """
        self.base_layout = layout

    @webapp2.cached_property
    def session(self):
        return self.session_store.get_session(backend="datastore")

    @webapp2.cached_property
    def messages(self):
        return self.session.get_flashes(key='_messages')

    @webapp2.cached_property
    def analytics_account_id(self):
        confs =conf.Configuracoes.get_conf(self.user_id)
        analytics_account_id = None
        if len(confs) > 0:
                analytics_account_id = confs[0].analytics_account_id
        return analytics_account_id

    @webapp2.cached_property
    def analytics_property_id(self):
        confs =conf.Configuracoes.get_conf(self.user_id)
        analytics_property_id = None
        if len(confs) > 0:
                analytics_property_id = confs[0].analytics_property_id
        return analytics_property_id

    @webapp2.cached_property
    def analytics_view_id(self):
        confs =conf.Configuracoes.get_conf(self.user_id)
        analytics_view_id = None
        if len(confs) > 0:
                analytics_view_id = confs[0].analytics_view_id
        return analytics_view_id

    @webapp2.cached_property
    def profile_id(self):
        confs =conf.Configuracoes.get_conf(self.user_id)
        profile_id = None
        if len(confs) > 0:
            if confs[0].analytics_view_id and confs[0].analytics_view_id != "None":
                profile_id = 'ga:'+confs[0].analytics_view_id
        return profile_id


    @webapp2.cached_property
    def loja_id(self):
        confs =conf.Configuracoes.get_conf(self.user_id)
        loja_id = None
        if len(confs) > 0:
                loja_id = confs[0].key.id()
        return loja_id

    @webapp2.cached_property
    def nome_loja(self):
        confs =conf.Configuracoes.get_conf(self.user_id)
        nome_loja = None
        if len(confs) > 0:
                nome_loja = confs[0].nome_loja
        return nome_loja

    @webapp2.cached_property
    def url_logo(self):
        confs =conf.Configuracoes.get_conf(self.user_id)
        url_logo_loja = None
        if len(confs) > 0:
                url_logo_loja = confs[0].url_logo
        return url_logo_loja

    @webapp2.cached_property
    def url_loja(self):
        confs =conf.Configuracoes.get_conf(self.user_id)
        url_loja = None
        if len(confs) > 0:
                url_loja = confs[0].url_loja
        return url_loja

    @webapp2.cached_property
    def get_merchant(self):
        confs =conf.Configuracoes.get_conf(self.user_id)
        url_xml = None
        if len(confs) > 0:
                url_xml = confs[0].merchant_url
        return url_xml

    def add_message(self, message, level=None):
        self.session.add_flash(message, level, key='_messages')


    def get_parameters_from_POST(cls, request):
        """
        Esse método recebe o método POST e pega todos
        os parâmetros enviados e os adiciona em um dicionário
        que depois poderá ser usado para complementar as ações
        em adicionar e
        :return: retorna um dict com os resultados encontrados.
        """
        params = {}
        for c, v in request.params.iteritems():
            params[c] = v

        return params

    def render_template(self, filename, **kwargs):

        # make all self.view variables available in jinja2 templates
        if hasattr(self, 'view'):
            kwargs.update(self.view.__dict__)

        # set or overwrite special vars for jinja templates
        kwargs.update({
            'google_tagmanager_code_head': self.app.config.get('google_tagmanager_code_head'),
            'google_tagmanager_code_body': self.app.config.get('google_tagmanager_code_body'),
            'app_name': self.app.config.get('app_name'),
            'url': self.request.url,
            'user': self.user_info,
            'user_plan': self.user_plan,
            'path': self.request.path,
            'analytics_account_id': self.analytics_account_id,
            'analytics_property_id': self.analytics_property_id,
            'analytics_view_id': self.analytics_view_id,
            'profile_id': self.profile_id,
            'loja_id': self.loja_id,
            'nome_loja': self.nome_loja,
            'url_logo': self.url_logo,
            'url_loja': self.url_loja,
            'query_string': self.request.query_string,
            'is_mobile': self.is_mobile,
            'enable_federated_login': self.app.config.get('enable_federated_login'),
            'base_layout': self.get_base_layout
        })

        locale = self.request.GET.get('locale', 'en_US')
        i18n.get_i18n().set_locale(locale)


        kwargs.update(self.auth_config)
        if hasattr(self, 'form'):
            kwargs['form'] = self.form
        if self.messages:
            kwargs['messages'] = []
            for m in self.messages:
                kwargs['messages'].append((m[0].decode('utf-8'), m[1]))

        self.response.headers.add_header('X-UA-Compatible', 'IE=Edge,chrome=1')
        self.response.write(self.jinja2.render_template(filename, **kwargs))


def user_required(handler):
        """
          Esse decorador deve ser usado no método da Request ( get\post\delete ), verifica se
          o usuário está logado e permite o acesso ao recurso.
        """
        def check_login(self, *args, **kwargs):
            auth = self.auth
            if not auth.get_user_by_session():
                self.redirect(self.uri_for('login'), abort=True)
            else:
                recurso = self.request.path
                free_features = [
                    '/listas-email', '/remover/email', r'/lista/*',
                    '/popups', '/conf', r''
                    '/payment-plan', '/payment-history', '/results', '/user',
                    '/meus-dados/senha',
                    r'/mailchimp*',
                    r'/api/*'

                ]


                """
                   Abaixo ele testa verifica se já foi feito a configuração do nome
                   da loja, que é o básico. Porém tem que testar melhor o objeto,
                   pesquisando por todos os dados necessários para comprar e vender.
                """
                if recurso == '/logout':
                    return handler(self, *args, **kwargs)
                if recurso != '/conf' and ( not self.loja_id or not self.url_loja ): # or not self.profile_id ):
                   self.add_message('ATENÇÃO: Para poder prosseguir é necessária a configuração dos campos com OBRIGATORIOS "*" ', 'danger')
                   return self.redirect_to('conf')

                #verifica o plano,
                #todo: melhorar isso aqui depois

                if self.user_plan == 'Free':
                    import re
                    allowed = False
                    for path in free_features:
                        if re.match(path, recurso):
                            allowed = True

                    if not allowed:
                        return self.redirect_to('conta-faturamento')

                if self.nome_loja and len(self.nome_loja) > 0:
                    return handler(self, *args, **kwargs)
                elif (self.nome_loja and len(self.nome_loja) < 1) and self.request.path != '/conf':
                    self.redirect_to('conf')
                else:
                    return handler(self, *args, **kwargs)
        return check_login
