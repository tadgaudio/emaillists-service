from jinja2.loaders import ChoiceLoader, FileSystemLoader
import webapp2
from webapp2_extras import jinja2 as j2x
from webapp2_extras import sessions

import utils
import jinja2
from markupsafe import Markup, escape_silent as escape


from webapp2_extras import i18n

from datetime import datetime
from dateutil import tz

# METHOD 1: Hardcode zones:
#from_zone = tz.gettz('UTC')
#to_zone = tz.gettz('America/New_York')

# METHOD 2: Auto-detect zones:
#from_zone = tz.tzutc()
#to_zone = tz.tzlocal()

#utc = datetime.utcnow()
#utc = datetime.strptime('2011-01-21 02:37:21', '%Y-%m-%d %H:%M:%S')

# Tell the datetime object that it's in UTC time zone since
# datetime objects are 'naive' by default
#utc = utc.replace(tzinfo=from_zone)

# Convert time zone
#central = utc.astimezone(to_zone)


def is_list(value):
    return isinstance(value, list)

def generate_csrf_token():
    session = sessions.get_store().get_session()
    if '_csrf_token' not in session:
        session['_csrf_token'] = utils.random_string()
    return session['_csrf_token']

def mark2html(eval_ctx):
    return Markup(eval_ctx.replace("\n", ''))

def format_currency(value):
    if value:
        try:
            value = float(value)
            _val = "%.2f" % value
        except ValueError:
            return value
    else:
        return 'None'

    _vals = _val.split('.')
    valor_final = _val.replace(".", ",")

    if len(_vals[0]) > 3:
       v_p = _vals[0][-3:]
       v_p_i =  _vals[0][:-3]
       valor_final = v_p_i+'.'+v_p+','+_vals[1]

    return valor_final

def convertTime(value):
    # METHOD 1: Hardcode zones:
    from_zone = tz.tzutc()
    to_zone = tz.tzlocal()

    # Tell the datetime object that it's in UTC time zone since
    # datetime objects are 'naive' by default
    utc = value.replace(tzinfo=from_zone)

    #  Convert time zone
    central = utc.astimezone(to_zone)
    return central

def format_datetime(value):
    return value.strftime('%d/%m/%Y %H:%M')

def include_raw(name):
    loader = ChoiceLoader([

            FileSystemLoader('template'),
        ])
    env = jinja2.Environment(loader=loader)
    env.globals['include_raw'] = include_raw
    return jinja2.Markup(loader.get_source(env, name)[0])

def include_file(name, params):
    loader = ChoiceLoader([
            FileSystemLoader('template'),
        ])
    env = jinja2.Environment(loader=loader)
    env.globals['include_raw'] = include_raw
    return jinja2.Template(loader.get_source(env, name)[0]).render(**params)


def jinja2_factory(app):

    j = j2x.Jinja2(app)

    j.environment = jinja2.Environment(
        loader=ChoiceLoader([
            FileSystemLoader('template'),
        ]),
        extensions=['jinja2.ext.autoescape', 'jinja2.ext.i18n'],
        autoescape=True)

    j.environment.install_gettext_translations(i18n)
    #jinja_env.filters['sanitize_html'] = mark2html
    j.environment.filters.update({
        'mark2html': mark2html,
        'convertTime': convertTime,
        'format_currency': format_currency,
        'format_datetime': format_datetime,
        'is_list': is_list,

    })
    j.environment.globals.update({
        # Set global variables.
        'csrf_token': generate_csrf_token,
        'uri_for': webapp2.uri_for,
        'getattr': getattr,
        'zip': zip,
        'include_raw': include_raw,
        'include_file': include_file
    })
    j.environment.tests.update({
        # Set test.
        # ...
    })
    return j
