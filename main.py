#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


#!/usr/bin/env python
# coding=utf-8
__author__ = 'tadgaudio@gmail.com (Tadeu luis Pires Gaudio)'

import os
import sys
import config
import routes

sys.path.insert(0, 'libs')

from webapp2_extras import jinja2
from libs import jinja_bootstrap
from google.appengine.api import namespace_manager
import webapp2

from model.listas import Lista
#from google.cloud import datastore
#client = datastore.Client(project=os.environ['APP_ID'])

webapp2_config = config.config

if not os.environ['SERVER_SOFTWARE'].startswith('Dev'):
    webapp2_config['devOps']['url'] = webapp2_config['devOps']['deploy_url']

app = webapp2.WSGIApplication(debug=os.environ['SERVER_SOFTWARE'].startswith('Dev'), config=webapp2_config)

# if not app.debug:
#   for status_int in app.config['error_templates']:
#       app.error_handlers[status_int] = handle_error


def handle_404(request, response, exception):
    response.set_status(404)
    j = jinja2.get_jinja2(factory=jinja_bootstrap.jinja2_factory, app=app)
    response.write(j.render_template('not-found.html'))


def handle_500(request, response, exception):
    response.write('A server error occurred!')
    response.set_status(500)

app.error_handlers[404] = handle_404
#app.error_handlers[500] = handle_500

routes.add_routes(app)


import model.conf as conf

class MainHandler(webapp2.RequestHandler):
    def get(self, namespace='default'):

        #params = {}

        lojas = conf.Configuracoes.get_all_active_stores()
        # Save the current namespace.
        previous_namespace = namespace_manager.get_namespace()
        try:
            namespace_manager.set_namespace(namespace)
            namespace_count = Lista.get_lists_by_loja_id('5629499534213120')
        finally:
            # Restore the saved namespace.
            namespace_manager.set_namespace(previous_namespace)

        self.response.write('Global: {}, Namespace {}: LOja {}'.format(namespace, namespace_count, lojas))
